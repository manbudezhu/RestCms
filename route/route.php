<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// Route::rule('路由表达式','路由地址','请求类型');
/*
 thinkphp5.1对路由参数进行了改进,使用了面向对象的方式
Route::get('new/:id','News/read',['ext'=>'html','https'=>true]);
Route::get('hello','admin/index/index')->https();
*/
use think\facade\Route;

Route::get('/','index/index/index');
//banner接口
Route::get('api/:version/banner/:id','api/:version.banner/getBanner');

//主题列表接口
Route::get('api/:version/theme','api/:version.theme/themeList');
//接口列表页接口
Route::get('api/:version/theme/:id','api/:version.theme/themeProduct');
//最近新品列表接口
Route::get('api/:version/newProduct','api/:version.product/newProduct');

//分类列表
Route::get('api/:version/category','api/:version.category/categoryList');
//某一类中的商品列表
Route::get('api/:version/categoryProduct','api/:version.product/getAllInCategory');
//商品详情
Route::get('api/:version/productInfo/:id','api/:version.product/productInfo')->pattern(['id'=>'\d+']);

/**************微信token接口************/
//微信token获取
Route::post('api/:version/token/user','api/:version.user/getToken');
/*微信token验证接口 @token token*/
Route::post('api/:version/token/verifyToken','api/:version.user/verifyToken');
//cms获取token的接口
Route::post('api/:version/token/app','api/:version.user/cmsToken');

/**************微信地址接口************/
//用户收货地址新增或者更新接口
Route::post('api/:version/address','api/:version.address/createOrUpdateAddress');
//查询用户地址接口
Route::get('api/:version/getUserAddress','api/:version.address/getAddress');

/*-----微信订单接口----*/
//下单接口
Route::post('api/:version/placeOrder','api/:version.order/placeOrder');
//订单列表 参数?page=1&size=3 token区分用户名
Route::get('api/:version/orderList','api/:version.order/orderList');
//订单详情
Route::get('api/:version/orderInfo/:orderId','api/:version.order/orderInfo');
//cms订单列表接口 获取所有的商品列表
Route::get('api/:version/order/paginate', 'api/:version.order/getSummary');
/*-----微信订单接口----*/
//支付接口
Route::post('api/:version/pay','api/:version.pay/prePay');
//支付回调接口
Route::post('api/:version/pay/notice','api/:version.pay/payNotice');
//微信回调转发接口,用于xdebug断点调试
Route::post('api/:version/pay/agentNotice','api/:version.pay/agentNotice');
//商品发货微信模板
Route::put('api/:version/deliver','api/:version.order/sendDeliverMsg');

//微信测试接口
Route::post('api/:version/test','index/index/test');


