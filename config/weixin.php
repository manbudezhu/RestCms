<?php
/**
 * descript:微信的配置文件
 * User: blue
 * Date: 2018/5/22 11:05
 */
return [
    'app_id'=>\think\facade\Env::get('wx.appId'),
    'app_secret'=>\think\facade\Env::get('wx.appSecret'),
    'openid_url'=>"https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code",
    'notice_url'=>"http://5139094d.ngrok.io//api/v1/pay/notice",

    //accessToken accessTok接口获取
    "accessToken"=>"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s"
];