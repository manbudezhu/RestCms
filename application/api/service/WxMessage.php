<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/8/1
 * Time: 20:44
 */

namespace app\api\service;


class WxMessage
{
    private $url="https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=%s";
    //接收者的openid
    protected $touser;
    //模板id
    protected $template_id;
    //跳转页面
    protected $page;
    //prpay_id
    protected $form_id;
    //发送微信消息模板
    protected $data;
    //构造函数初始化
    public function __construct()
    {
        $token=AccessToken::getAccessToken();
        $this->url=sprintf($this->url,$token);
    }

    /**
     * 微信模板消息
     * @param $openId
     * @return array
     */
    public function sendMessage($openId){
        $message=[
            'touser'=>$openId,
            'template_id'=>$this->template_id,
            'page'=>$this->page,
            'form_id'=>$this->form_id,
            'data'=>$this->data,
        ];
        $message=json($message);
        $result=curl_request($this->url,"post",'',$message);
        $result=json_decode($result,true);
        $code=$result['errcode']?0:1;
        return [
            'code'=>$code,
            'msg'=>$result['errmsg']
        ];
    }


}