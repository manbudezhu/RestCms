<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/8/1
 * Time: 12:07
 */

namespace app\api\service;


class CmsToken extends Token
{
     public function getCmsToken($data){
        $token=$this->produceToken();
        $result['uid']=$data['id'];
        $result['scope']=16;
        $resultJ=json($result);
        $expire=config('security.token_expire');
        cache($token,$resultJ,$expire);
        return $token;
    }
}