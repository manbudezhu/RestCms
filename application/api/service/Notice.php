<?php
/**
 * Describe:
 * Source:
 * User: blue
 * Time: 2018/6/15 2:14
 */

namespace app\api\service;
use app\api\model\Product;
use app\lib\enum\PayEnum;
use think\Db;
use think\facade\Env;
use think\facade\Log;

require_once Env::get('root_path') . 'extend/WxPay/WxPay.Api.php';

class Notice extends \WxPayNotify
{
    /**
     * 重载sdk微信支付回调处理函数
     * @param array $data 回调解释出的参数
     * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
     * @return true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
     */

    public function NotifyProcess($data, &$msg){
        /*1.库存量检测
         * 2.更新订单状态
         * 3.减库存*/
        Db::startTrans();
        try{
            //文件锁开始,参考oneNote,最好用消息队列
            //支付状态
            $order = \app\api\model\Order::where('order_no', '=', $data['out_trade_no'])->find();
            if($data['result_code']){
                $stockStatus=(new Order())->getOrderStock($order->id);
                //库存量检测
                if($stockStatus['pass']){
                    //更改状态为已支付
                    $this->updateOrderStatus($order->id,true);
                    //进行减少库存量
                    $this->reduceStock($stockStatus['pStatusArray']);
                }else{
                    //更改支付状态为PAID_BUT_OUT_OF(已支付,单库存量不足)
                    $this->updateOrderStatus($data['out_trade_no'],false);
                }
            }
            Db::commit();
            //文件锁结束
        }catch(\Exception $e){
            Db::rollback();
            Log::write($e->getMessage(),'error');
            return false;
        }
        return true;
    }

    /**
     * 更新指定订单的状态
     * @param $orderNo
     * @param $bool
     */
    private function updateOrderStatus($orderid,$sel){
        $status=$sel?PayEnum::PAID:PayEnum::PAID_BUT_OUT_OF;
        \app\api\model\Order::where('id','=',$orderid)->update(["status"=>$status]);

    }

    /**
     * 减库存量
     * @param $order
     */
    private function reduceStock($products){
        foreach ($products as $product){
            Product::where('id','=',$product['id'])->setDec('stock',$product['count']);
        }
    }

}