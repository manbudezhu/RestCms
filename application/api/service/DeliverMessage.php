<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/8/2
 * Time: 9:37
 */

namespace app\api\service;


use app\api\model\UserAddress;
use app\lib\enum\PayEnum;
use app\lib\exception\ParamException;

class DeliverMessage extends WxMessage
{
    //发货模板id
    const deliverTemplateCode="PUePq0IiFh4-n612e-5QiGSewJpx12WT8qFSE4SrBPg";

    public function deliverMessage($order,$pageJump=""){
        //设置成员属性
        $this->template_id=self::deliverTemplateCode;
        $this->page=$pageJump;
        $this->form_id=$order->prepay_id;
        //消息模板的数据
        $this->data=$this->msgFormat($order);
        //从order对象中获取openid
        $openid=$this->getOpenId($order->uid);
        //发送发货消息,需要判断用户已经付款
        if($order->status!=PayEnum::PAID){
            throw new ParamException(['msg'=>'请支付之后再发货']);
        }
        //发送消息
        return parent::sendMessage($openid);
    }

    public function getOpenId($uid){
        //通过uid到user表中获取openId
        return UserAddress::get($uid)->visible('openid');
    }

    public function msgFormat($order){
        $data=[
            "keyword1"=>['value'=>"顺丰速递"],
            "keyword2"=>['value'=>$order->sanp_name],
            "keyword3"=>['value'=>$order->order_no],
            "keyword4"=>['value'=>date('Y-m-d H:i',time())],
        ];
        return $data;
    }
}