<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/8/2
 * Time: 14:19
 */

namespace app\api\behavior;


class Cross
{
    /**
     * 跨域的行为处理
     * @param $params
     */
    public function run($params){
        //指定允许访问的域名,*为全部可以访问
        header("ALLOW-CONTROL-ALLOW-ORIGIN:*");
        // 响应类型
        header('Access-Control-Allow-Methods:POST,GET');
        // 响应头设置
        header('Access-Control-Allow-Headers:token,Origin,x-requested-with,content-type');
        //如果请求类型为options则不响应
        if(request()->isOptions()){
            exit();
        }
    }
}