<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/11 10:54
 */

namespace app\api\behavior;

class Log
{
    public function run($params){
        // 手动开启log日志
        if(method_exists(__CLASS__,'logInit')) $this->logInit();
    }
    /**
     * 日志初始化
     */
    private function logInit(){
        \think\facade\Log::init([
            'type'=>'file',
            'path'=> \think\facade\Env::get('root_path').'/log',
            'level'=>['sql','error']
        ]);
    }
}