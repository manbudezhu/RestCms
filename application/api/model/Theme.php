<?php

namespace app\api\model;

use think\Model;

class Theme extends Model
{
    protected $hidden=['update_time','delete_time'];
    //获取topicImg
    public  function topicImg(){
       return $this->belongsTo('Image','topic_img_id','id');
    }
    //获取headImg图片
    public function headImg(){
        Return $this->belongsTo('Image','head_img_id','id');
    }
    /**
     * 多对多关系
     */
    public function productList(){
        Return $this->belongsToMany('product','theme_product','theme_id','product_id');
    }
}
