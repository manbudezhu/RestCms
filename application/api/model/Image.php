<?php

namespace app\api\model;

class Image extends Base
{
    protected $visible=['url'];
    /**
     * 图片地址拦截器
     * @param $value
     * @param $data
     * @return string
     */
    public function getUrlAttr($value,$data){
        return $this->imgUrl($value,$data);
    }
}
