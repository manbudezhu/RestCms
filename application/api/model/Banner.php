<?php

namespace app\api\model;

use think\Model;

class Banner extends Model
{
    protected $hidden=['delete_time','update_time'];
    //查找banner对应的items表
    public function items(){
        return $this->hasMany('BannerItem','banner_id','id');
    }
    //获取轮播图数据
    static public function getBanner($data){
        return self::with(['items','items.img'])->find($data);
    }
}
