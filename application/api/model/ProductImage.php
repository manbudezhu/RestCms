<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/28 10:08
 */

namespace app\api\model;

class ProductImage extends Base
{
    protected $hidden=['deldete_time'];

    public function withImage(){
        return $this->belongsTo('Image','img_id','id');
    }
}