<?php

namespace app\api\model;

class User extends Base
{
    protected $autoWriteTimestamp = true;
    static public function findOpenId($openid){
        $res=self::where('openid',$openid)->find();
        return $res;
    }
}
