<?php
/**
 * Describe:
 * Source:
 * User: blue
 * Time: 2018/5/21 19:57
 */

namespace app\api\model;


class Category extends Base
{
    protected $hidden=['delete_time','update_time'];
    public function topicImg(){
        return $this->belongsTo('Image','topic_img_id','id');
    }
}