<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/28 15:44
 */

namespace app\api\model;

use app\api\service\UserToken;


class UserAddress extends Base
{

    public static function AddressCU($address)
    {
        $uid = UserToken::tokenGetUid();
        /*查看用户地址表中有该用户的地址没->如果没有则新增->否则更新*/
        $user = self::where('user_id', $uid)->find();
        if($user){
           //更新数据库
            $res=self::where('user_id',$uid)->update($address);
        }else{
           //写入数据库
            $address['user_id']=$uid;
            $res=self::create($address);
        }
        return $res;
    }
}