<?php

namespace app\api\model;

use think\Model;

class Base extends Model
{
    /**
     * 基类图片地址拦截器方法
     * @param $value
     * @param $data
     * @return string
     */
    protected function imgUrl($value,$data){
        $file=$value;
        if($data['from']==1){
            //    配置文件的img前缀+$value
            $file=config('api.imgUrlPrefix').$value;
        }
        return $file;
    }
}
