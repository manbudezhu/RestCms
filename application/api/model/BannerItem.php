<?php

namespace app\api\model;

use think\Model;

class BannerItem extends Model
{
    protected $hidden=['delete_time','update_time'];
    //查找item 对应的图片表信息
    public function img(){
        return $this->belongsTo('Image','img_id','id');
    }
}
