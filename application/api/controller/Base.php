<?php

/**
 * descript:基类
 * User: blue
 * Date: 2018/5/30 10:20
 */

namespace app\api\controller;

use app\api\service\UserToken;
use think\Controller;

class Base extends Controller
{
    //token user用户权限检查
    protected function checkUserScope(){
        UserToken::userApprove();
    }
    //token super用户权限检测
    protected function checkSuperScope(){
        UserToken::superApprove();
    }
    //token 通用用户检测
    protected function checkCommonScope(){
        UserToken::commonApprove();
    }
}