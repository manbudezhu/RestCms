<?php

/**
 * descript: banner 控制器
 * User: blue
 * Date: 2018/5/3 10:48
 */

namespace app\api\controller\v1;
use \app\api\model\Banner as BannerModel;
use app\api\validate\IntegerValidate;
use app\lib\exception\BannerMissExcepthion;

class Banner
{
    /**
     * @param $id banner id
     * @return array|bool|null|\PDOStatement|string|\think\Model
     * @throws \think\Exception
     */
    public function getBanner($id){
        $data=['id'=>intval($id)];
        //调用验证层进行验证
        (new IntegerValidate())->goCheck($data);
        // $res=BannerModel::with(['items','items.img'])->find(1);
        $res=BannerModel::getBanner($data);
        //如果资源不存在抛出异常
        if(!$res){
            throw new BannerMissExcepthion();
        }
        return $res;
    }
}