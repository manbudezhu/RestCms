<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/30 10:51
 */

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\service\Token;
use app\api\service\UserToken;
use app\api\validate\IntegerValidate;
use app\api\validate\OrderValidate;
use app\api\service\Order as OrderServer;
use think\facade\Request;

class Order extends Base
{
    //前置方法,检查Token权限
    protected $beforeActionList =[
        "checkUserScope"=>["only"=>"placeorder"]
    ];
    /**
     * 下单接口
     */
    public function placeOrder(){
        $oProducts=Request::post('products/a');
        (new OrderValidate())->goCheck(['products'=>$oProducts]);
        $uid=UserToken::tokenGetUid();
        $order=new OrderServer();
        $status=$order->Place($uid,$oProducts);
        return $status;
    }

    /**
     * 订单列表
     */
    public function orderList($page=1,$size=15){
        $uid=Token::tokenGetInfo('uid');
        $check=(new IntegerValidate())->goCheck(['id'=>$uid]);
        if($check){
           return $status=\app\api\model\Order::orderPage($uid,$size,$page);
        }
    }

    /**
     * @param $orderId 订单编号
     * @return array 数据
     */
    public function orderInfo($orderId){
        $uid=Token::tokenGetInfo('uid');
        $check=(new IntegerValidate())->goCheck(['id'=>$orderId]);
        if($check){
            $res=\app\api\model\Order::orderInfo($uid,$orderId);
            return $res['data'];
        }
    }

    /**
     *
     */
    public function getSummary($page=1,$size=20){
        $pagingOrders = \app\api\model\Order::getSummaryByPage($page, $size);
        if ($pagingOrders->isEmpty())
        {
            return [
                'current_page' => $pagingOrders->currentPage(),
                'data' => []
            ];
        }
        $data = $pagingOrders->hidden(['snap_items', 'snap_address'])
            ->toArray();
        return [
            'current_page' => $pagingOrders->currentPage(),
            'data' => $data
        ];
    }

    /**
     * 微信发货消息推送
     */
    public function sendDeliverMsg($id){
        (new IntegerValidate())->goCheck($id);
        $res=\app\api\model\Order::sendDeliversMsgM($id);
        return $res;
    }


}