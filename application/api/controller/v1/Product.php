<?php

namespace app\api\controller\v1;
use app\api\model\Product as ProductModel;
use app\api\validate\CountValidate;
use app\api\validate\IntegerValidate;
use app\lib\exception\ProductException;

class Product
{
    /**
     * 最近新品接口
     */
    public function newProduct($count=15){
        (new CountValidate())->goCheck(['count'=>$count]);
        $res=ProductModel::findNew($count);
        if($res->isEmpty()||!$res){
            throw new ProductException();
        }
        return $res;
    }
    /**
     *获取所有的该类中商品
     */
    public function getAllInCategory($id){

        (new IntegerValidate())->goCheck(['id'=>$id]);
        $res=ProductModel::ProductInCategory($id);
        if($res->isEmpty()){
            throw new ProductException();
        }
        $res->hidden(['stock']);
        return $res;
    }

    public function productInfo($id){
        (new IntegerValidate())->goCheck(['id'=>$id]);
        $res=ProductModel::getProductInfo($id);
        return $res;
    }
}
