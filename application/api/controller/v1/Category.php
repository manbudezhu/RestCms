<?php
/**
 * Describe:
 * Source:
 * User: blue
 * Time: 2018/5/21 19:53
 */
namespace app\api\controller\v1;
use app\api\model\Category as CategoryModel;
use app\lib\exception\CateException;

class Category
{
    /**
     * category 获取category列表
     */
    public function CategoryList(){
        $res=CategoryModel::all([],['topicImg']);
        if($res->isEmpty()){
            throw new CateException();
        }
        return $res;
    }

}