<?php

/**
 * descript:
 * User: blue
 * Date: 2018/6/8 9:17
 */

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\service\Notice;
use app\api\service\PayS;
use app\api\validate\IntegerValidate;
use think\facade\Log;

class Pay extends Base
{
    //权限控制
    protected $beforeActionList=[
        "checkUserScope"=>["only"=>"prePay"]
    ];
    /**
     * 订单预付款
     * @param string $id 订单号(主键)
     */
    public function prePay($id=""){
        (new IntegerValidate())->goCheck(["id"=>$id]);
        $payServer=new PayS($id);
        $status=$payServer->Pay();
        return $status;
    }

    /*
     *订单回调方法[微信每隔15s,15s,30s,180/1800.....访问该接口,直至成功为止]
     */
    public function agentNotice(){
         $notify=new Notice();
         $notify->handle();
    }
    /*扩展网页代理访问使用Sessionstart调试的方法*/
    public function payNotice(){
/*        $notify=new Notice();
        $notify->handle();*/
        $xmlData=file_get_contents("php://input");
        return $this->curl_agent_xdebug("http://5139094d.ngrok.io/api/v1/pay/agentNotice?XDEBUG_SESSION_START=12523",$xmlData);
    }

    //访问代理转发
    public function curl_agent_xdebug($url,$rawData){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $rawData);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: text'
            )
        );
        $data = curl_exec($ch);
        curl_close($ch);
        return ($data);
    }


}