<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/5/17
 * Time: 8:10
 */

namespace app\api\controller\v1;

use app\api\validate\IntegerValidate;
use app\api\validate\ThemeValidate;
use app\lib\exception\ThemeException;

class Theme
{
    /**
     * 专题列表接口
     */
    public function themeList(){
        // 接口获取专题列表
        // 'api/v1/theme?ids='
        $ids=request()->param();
        if(!empty($ids)) (new ThemeValidate())->goCheck($ids);
        //获取专题数据
        $res=\app\api\model\Theme::with(['topicImg','headImg'])->select($ids['ids']);
        //$res为不存在时候返回空数组或者false两种情况
        if(count($res)<=0||!$res){
            //抛出主题未找到异常层
            throw new ThemeException();
        }
        return $res;
    }
    /**
     * 返回该专题内所有的商品信息
     */
    public function themeProduct($id){
        $id=intval($id);
        (new IntegerValidate())->goCheck(['id'=>$id]);
        //验证参数
        $res=\app\api\model\Theme::with(['productList','topicImg','headImg'])->find($id);
        //数据没有找到,抛出异常
        if(count($res)<=0||!$res){
            throw new ThemeException();
        }
        return $res;
    }


}