<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/28 15:38
 */

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\model\UserAddress;
use app\api\service\Token;
use app\api\service\UserToken;
use app\api\validate\AddressValidate;

use think\facade\Request;
use app\lib\exception\SuccessMessage;
use think\Exception;

class Address extends Base
{
    protected $beforeActionList=[
        'checkCommonScope'=>['only','createOrUpdateAddress']
    ];
    /**
     * 创建或更新收货地址接口
     * @throws Exception
     * @throws \app\lib\exception\ParamException
     */
    public function createOrUpdateAddress(){
        $address=Request::post();
        if(empty($address)){
            throw new Exception('没有从客户端接收到数据');
        }
        $valid=new AddressValidate();
        $valid->goCheck($address);
        $filterDate=$valid->paramByRules($address);
        //模型的地址处理方法
        $res=UserAddress::AddressCU($filterDate);
        // if(!$res) {throw new Exception(['message'=>'记录写入失败']);}
        //抛出成功反馈信息
        throw json(new SuccessMessage(),201);
    }

    public function getAddress(){
        $uid=Token::tokenGetInfo('uid');
        $address=UserAddress::where('user_id',$uid)->find();
        return $address;
    }

}