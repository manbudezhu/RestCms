<?php
/**
 * Describe:基础验证层
 * Source:
 * User: blue
 * Time: 2018/5/3 20:13
 */

namespace app\api\validate;


use app\lib\exception\ParamException;
use think\Exception;
use think\Validate;

class BaseValidate extends Validate
{
    /**
     * 通用验证方法
     * @param array $data 验证的数据
     * @return bool
     * @throws Exception
     */
    public function goCheck(array $data){
        if(empty($data)) throw new ParamException(['msg'=>'数据参数不能为空']);
        $check=$this->batch()->check($data);
        if(!$check){
            $error=json_encode($this->error,JSON_UNESCAPED_UNICODE);
            //抛出参数验证异常层
            throw new ParamException(['msg'=>$error]);
        }else{
            return true;
        }
    }

    /**
     * 必须是正整数验证规则
     * @param $value
     * @param string $rule
     * @param string $data
     * @param string $field
     * @return bool
     */
    protected function mustPositive($value,$rule="",$data="",$field=""){
        if(is_numeric($value)&&is_int($value+0)&&($value+0)>0){
            return  true;
        }else{
            return false;
        }
    }
    /**
     * 检查ids参数是否符合标准1,2,3
     * @param $value
     * @return bool
     */
    protected function checkIDs($value){
        $values=explode(',',$value);
        if(empty($value)){
            return false;
        }
        foreach ($values as $id){
            if(!$this->mustPositive($id)){
                return false;
            }
        }
        return true;
    }

    public function isMobile($value){
        $res=preg_match('/^1[3,4,5,7,8]\d{9}$/',$value);
        if($res){
            return true;
        }else{
            return false;
        }
    }
    /**
     * @param $value
     * @return bool
     */
    protected function isNotEmpty($value){
        if(empty($value)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 根据验证规则获取指定数据
     * 不允许传递uid 或者user_id,因为是从token获取的
     */
    public function paramByRules($arr){
        if(array_key_exists('uid',$arr)||array_key_exists('user_id',$arr)){
            throw new ParamException(['msg'=>'传入非法的参数uid或user_id']);
        }
        $filterArr=[];
        foreach ($this->rule as $k=>$v){
            $filterArr[$k]=$arr[$k];
        }
        return $filterArr;
    }
}