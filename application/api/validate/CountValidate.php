<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/21 15:42
 */

namespace app\api\validate;
/**
 * Class CountValidate 最近新品验证
 * @package app\api\validate
 */
class CountValidate extends BaseValidate
{
    protected $rule=[
        'count'=>'mustPositive|between:1,20'
    ];
    protected $message=[
      'count.mustPositive'=>'参数必须为正整数'
    ];

}