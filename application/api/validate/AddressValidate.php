<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/28 15:41
 */

namespace app\api\validate;

class AddressValidate extends BaseValidate
{
    protected $rule=[
        'name'=>'require|isNotEmpty',
        'mobile'=>'require|isMobile',
        'province'=>'require|isNotEmpty',
        'city'=>'require|isNotEmpty',
        'country'=>'require|isNotEmpty'
    ];
    protected $message=[
      "mobile.isMobile"=>'手机号有误'
    ];
}