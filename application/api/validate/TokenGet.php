<?php
/**
 * Describe:
 * Source:
 * User: blue
 * Time: 2018/5/26 14:33
 */

namespace app\api\validate;


class TokenGet extends BaseValidate
{
    protected $rule=[
        'code'=>'require|isNotEmpty'
    ];
    protected $message=[
        'code.isNotEmpty'=>'code不能为空'
    ];
}