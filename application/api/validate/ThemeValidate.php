<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/5/17
 * Time: 8:29
 */

namespace app\api\validate;

class ThemeValidate extends BaseValidate
{
    protected  $rule=[
        'ids'=>'require|checkIDs'
    ];
    protected $message=[
        'ids.checkIDs'=>'ids参数必须是以逗号分隔的正整数'
    ];


}