<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/8/1
 * Time: 11:51
 */

namespace app\api\validate;


class CmsUserValidate extends BaseValidate
{
    protected $rule=[
        'userName'=>'require',
        'pwd'=>'require'
    ];
    protected $message=[
        'ac'=>'用户名不能为空',
        'se'=>"密码不能为空"
    ];
}