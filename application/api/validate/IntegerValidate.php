<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/3 16:13
 */

namespace app\api\validate;

class IntegerValidate extends BaseValidate
{
    protected $rule=[
      'id'=>'require|mustPositive'
    ];
    protected $message=[
      'id.mustPositive'=>'参数值不为正整数'
    ];

}