<?php

/**
 * descript: scope 权限enum常量
 * User: blue
 * Date: 2018/5/30 9:17
 */

namespace app\lib\enum;

class ScopeEnum
{
    const user=16;
    const super=32;
}