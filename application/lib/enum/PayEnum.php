<?php

/**
 * descript:
 * User: blue
 * Date: 2018/6/8 15:20
 */

namespace app\lib\enum;

class PayEnum
{
    //待支付
    const UNPAID=1;
    //已支付
    const PAID=2;
    //已发货
    const DELIVERED=3;
    //已支付,但是库存不足
    const PAID_BUT_OUT_OF=4;

}