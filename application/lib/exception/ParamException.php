<?php

/**
 * descript:验证层抛出的接口参数异常
 * User: blue
 * Date: 2018/5/10 15:41
 */

namespace app\lib\exception;

class ParamException extends BaseException
{
    public $code=400;
    public $msg='params_error';
    public $errCode=10000;

}