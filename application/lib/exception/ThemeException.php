<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/5/17
 * Time: 9:07
 */

namespace app\lib\exception;


class ThemeException extends BaseException
{
    public $code=400;
    public $msg='专题没有找到';
    public $errCode='40001';
}