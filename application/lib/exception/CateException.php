<?php
/**
 * descript:
 * User: blue
 * Date: 2018/5/21 17:04
 */

namespace app\lib\exception;


class CateException extends BaseException
{
    public $code=404;
    public $msg='category_not_find';
    public $errCode=60000;
}