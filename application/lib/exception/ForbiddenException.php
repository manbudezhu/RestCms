<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/30 15:54
 */

namespace app\lib\exception;

class ForbiddenException extends BaseException
{
    public $code='401';
    public $msg='没有权限访问';
    public $errCode='20001';
}