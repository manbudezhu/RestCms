<?php

/**
 * descript:接口基础自定义异常类
 * User: blue
 * Date: 2018/5/7 10:58
 */

namespace app\lib\exception;

use think\Exception;

class BaseException extends Exception
{
    //HTTP 状态码 404,200
    public $code=400;
    //错误具体信息
    public $msg="custom_error";
    //自定义的错误码
    public $errCode=10000;
    //构造函数初始化类属性
    public function __construct(array $conf=[])
    {
        //防御性代码
        if(!is_array($conf)) return;
        //属性初始化
        array_key_exists('msg',$conf)&&$this->msg=$conf['msg'];
        array_key_exists('errCode',$conf)&&$this->errCode=$conf['errCode'];
        array_key_exists('code',$conf)&&$this->url=$conf['code'];
    }
}