<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/28 16:01
 */

namespace app\lib\exception;

class TokenException extends BaseException
{
    public $code='401';
    public $msg='token过期或者无效';
    public $errCode='10001';
}