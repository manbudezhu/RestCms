<?php
/**
 * Created by PhpStorm.
 * User: blue
 * Date: 2018/5/24
 * Time: 10:34
 */

namespace app\lib\exception;


class WxException extends BaseException
{
    public $code=401;
    public $msg='微信接口返回错误';
    public $errCode='60001';
}