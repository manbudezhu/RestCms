<?php
/**
 * Describe:
 * Source:
 * User: blue
 * Time: 2018/5/27 11:41
 */

namespace app\lib\exception;


class CacheException extends BaseException
{
    public $code=401;
    public $msg='cache_write_error';
    public $errCode=10005;
}