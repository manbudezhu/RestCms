<?php

/**
 * descript:
 * User: blue
 * Date: 2018/5/29 11:13
 */

namespace app\lib\exception;

class SuccessMessage extends BaseException
{
    public $code=201;
    public $msg='ok';
    public $errCode=0;
}