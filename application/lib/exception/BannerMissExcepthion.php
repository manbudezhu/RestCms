<?php

/**
 * descript:banner没有找到的异常
 * User: blue
 * Date: 2018/5/10 15:41
 */

namespace app\lib\exception;

class BannerMissExcepthion extends BaseException
{
    public $code=404;
    public $msg='uri_not_find';
    public $errCode=10001;

}