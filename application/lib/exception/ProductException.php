<?php
/**
 * descript:
 * User: blue
 * Date: 2018/5/21 17:04
 */

namespace app\lib\exception;


class ProductException extends BaseException
{
    public $code=400;
    public $msg='product_not_find';
    public $errCode=50000;
}